package queue

import "errors"

var IsEmpty = errors.New("queue is empty")

type Queue struct {
	content []interface{}
}

func (q *Queue) Enqueue(value interface{}) {
	q.content = append(q.content, value)
}

func (q *Queue) First() (interface{}, error) {
	if len(q.content) <= 0 {
		return nil, IsEmpty
	}
	value := q.content[0]
	return value, nil
}

func (q *Queue) Dequeue() (interface{}, error) {
	value, err := q.First()
	if err != nil {
		return nil, err
	}
	q.content = q.content[1:]
	return value, nil
}

func (q *Queue) Length() int {
	return len(q.content)
}
