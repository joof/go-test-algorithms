package heap

import (
	"fmt"
)

func parentIndex(index int) int{
	return (index - 1)/2
}

func childrenIndex(index int) (int, int) {
	return 2*index + 1, 2*index + 2
}


type PriorityQueue struct {
	values []interface{}

	isBigger func(a, b interface{}) bool
}

func (pq *PriorityQueue) SetCompareFunction(isBigger func(a, b interface{}) bool) {
	pq.isBigger = isBigger
}

func (pq *PriorityQueue) checkIndex(index int) error {
	if index < 0 || index >= pq.Len() {
		return fmt.Errorf("index %d out of range", index)
	}
	return nil
}

func (pq *PriorityQueue) swap(index1, index2 int) error{
	if err := pq.checkIndex(index1); err != nil {
		return err
	}
	if err := pq.checkIndex(index2); err != nil {
		return err
	}
	pq.values[index1], pq.values[index2] = pq.values[index2], pq.values[index1]
	return nil
}

func (pq *PriorityQueue) heapify(index int) error{
	if err := pq.checkIndex(index); err != nil {
		return err
	}
	biggestValue := pq.values[index]
	biggestIndex := index
	leftIndex, rightIndex := childrenIndex(index)
	if err := pq.checkIndex(leftIndex); err == nil {
		leftVal := pq.values[leftIndex]
		if pq.isBigger(leftVal, biggestValue) {
			biggestValue = leftVal
			biggestIndex = leftIndex
		}
	}
	if err := pq.checkIndex(rightIndex); err == nil {
		rightVal := pq.values[rightIndex]
		if pq.isBigger(rightVal, biggestValue) {
			biggestValue = rightVal
			biggestIndex = rightIndex
		}
	}

	if biggestIndex != index {
		_ = pq.swap(index, biggestIndex)
		_ = pq.heapify(biggestIndex)
	}
	return nil
}

func (pq *PriorityQueue) Len() int {
	return len(pq.values)
}

func (pq *PriorityQueue) Insert(value interface{}) {
	pq.values = append(pq.values, value)
	newIndex := pq.Len() - 1
	_ = pq.swap(0, newIndex)
	_ = pq.heapify(0)
}

func (pq *PriorityQueue) First() (interface{}, error) {
	if pq.Len() <= 0 {
		err := fmt.Errorf("heap is empty")
		return nil, err
	}
	return pq.values[0], nil
}

func (pq *PriorityQueue) Pop() (interface{}, error){
	value, err := pq.First()
	if err != nil {
		return nil, err
	}
	lastIndex := pq.Len() - 1
	_ = pq.swap(0, lastIndex)
	pq.values = pq.values[:lastIndex]
	_ = pq.heapify(0)
	return value, nil
}

func (pq *PriorityQueue) Get(index int) (interface{}, error){
	if err := pq.checkIndex(index); err != nil {
		return nil, err
	}
	return pq.values[index], nil
}

func (pq *PriorityQueue) Index(value interface{}) (int, error){
	for index, val := range pq.values{
		if val == value {
			return index, nil
		}
	}
	err := fmt.Errorf("value %v not exist", value)
	return -1, err
}