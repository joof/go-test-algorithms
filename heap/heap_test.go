package heap

import (
	"github.com/stretchr/testify/assert"
	"math"
	"math/rand"
	"sort"
	"testing"
)

func inIsBigger(a, b interface{}) bool {
	return a.(int) > b.(int)
}

func setUpHeap(len, step int) *PriorityQueue {
	min := -len/2 * step - 1
	max := len/2 * step
	pq := &PriorityQueue{isBigger: inIsBigger}
	for i := max; i > min; i -= step {
		pq.values = append(pq.values, i)
	}
	return pq
}

func TestPriorityQueue_First(t *testing.T) {
	pq := setUpHeap(0, 0)
	first, err := pq.First()
	assert.NotNil(t, err)
	assert.Nil(t, first)

	pq = setUpHeap(10, 2)
	first, err = pq.First()
	assert.Nil(t, err)
	assert.Equal(t, first, 5)

	pq = setUpHeap(12, 3)
	first, err = pq.First()
	assert.Nil(t, err)
	assert.Equal(t, first, 6)

	pq = setUpHeap(18, 6)
	first, err = pq.First()
	assert.Nil(t, err)
	assert.Equal(t, first, 9)

}

func TestPriorityQueue_Pop(t *testing.T) {
	for try := 0; try < 10; try++{
		length := rand.Intn(100) + 1
		step := rand.Intn(5) + 1
		pq := setUpHeap(length, step)

		oldValue := math.MaxInt64
		for pq.Len() > 0 {
			newVal, err := pq.Pop()
			assert.Nil(t, err)
			assert.LessOrEqual(t, newVal, oldValue)
		}
		_, err := pq.Pop()
		assert.NotNil(t, err)
	}
}

func TestPriorityQueue_Insert(t *testing.T) {
	values := make([]int, 0)
	pq := setUpHeap(0, 0)
	for i := 0; i < 30; i++{
		newVal := rand.Int()
		pq.Insert(newVal)
		values = append(values, newVal)
	}
	sort.Ints(values)
	for len(values) > 0 {
		biggestValue := values[len(values) - 1]
		values = values[:len(values) - 1]
		heapBiggestValue, err := pq.Pop()
		assert.Nil(t, err)
		assert.Equal(t, biggestValue, heapBiggestValue)
	}
	_, err := pq.Pop()
	assert.NotNil(t, err)
}