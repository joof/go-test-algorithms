package tree

import (
	"fmt"
	"testing"
)

func TestBinaryTree_String(t *testing.T) {
	isbigger := func(a, b interface{}) bool {
		aint := a.(int)
		bint := b.(int)
		return aint > bint
	}
	tree := &BinaryTree{}
	tree.SetCompareFunction(isbigger)
	tree.Add(0)
	tree.Add(5)
	tree.Add(7)
	tree.Add(-5)
	tree.Add(-3)
	tree.Add(-4)
	tree.Add(-6)
	tree.Add(3)
	tree.Add(6)
	fmt.Println(tree.Exists(3))
	fmt.Println(tree.String())
}
