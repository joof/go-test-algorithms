package tree

import "fmt"

type Node struct {
	Value  interface{}
	Left   *Node
	Right  *Node
	Parent *Node
}

func (n *Node) IsLeaf() bool {
	return n.Left == nil && n.Right == nil
}

type BinaryTree struct {
	root     *Node
	isBigger func(firstVal, secondVal interface{}) bool
}

func (t *BinaryTree) SetCompareFunction(isBigger func(firstVal, secondVal interface{}) bool) {
	t.isBigger = isBigger
}

func (t *BinaryTree) Add(value interface{}) {
	if t.root == nil {
		t.root = &Node{Value: value}
		return
	}
	tempNode := t.root
	currNode := new(Node)
	for tempNode != nil {
		currNode = tempNode
		if t.isBigger(currNode.Value, value) {
			tempNode = currNode.Left
		} else {
			tempNode = currNode.Right
		}
	}

	tempNode = &Node{Value: value, Parent: currNode}
	if t.isBigger(currNode.Value, tempNode.Value) {
		currNode.Left = tempNode
	} else {
		currNode.Right = tempNode
	}
}

func (t *BinaryTree) Exists(value interface{}) bool {
	currNode := t.root
	for currNode != nil {
		if currNode.Value == value {
			return true
		}
		if t.isBigger(currNode.Value, value) {
			currNode = currNode.Left
		} else {
			currNode = currNode.Right
		}
	}
	return false
}

type nodeDepth struct {
	Node
	depth int
}

func (nd *nodeDepth) set(n *Node, depth int) *nodeDepth {
	nd.Value = n.Value
	nd.Right = n.Right
	nd.Left = n.Left
	nd.Parent = n.Parent
	nd.depth = depth
	return nd
}

func (t BinaryTree) String() string {
	root := &nodeDepth{}
	root.set(t.root, 0)
	var queue []*nodeDepth
	nodesInString := ""
	queue = append(queue, root)
	for len(queue) > 0 {
		node := queue[0]
		queue = queue[1:]
		if node.Left != nil {
			newNode := &nodeDepth{}
			newNode.set(node.Left, node.depth+1)
			queue = append(queue, newNode)
		}
		if node.Right != nil {
			newNode := &nodeDepth{}
			newNode.set(node.Right, node.depth+1)
			queue = append(queue, newNode)
		}
		if node.Parent == nil {
			nodesInString += fmt.Sprintf("{value: %v, depth: %d}", node.Value, node.depth)
		} else {
			nodesInString += fmt.Sprintf("{value: %v, depth: %d, parent: %v}",
				node.Value, node.depth, node.Parent.Value)
		}
		if len(queue) > 0 {
			if node.depth < queue[0].depth {
				nodesInString += fmt.Sprintf("\n")
			}
		}
	}
	return nodesInString
}
