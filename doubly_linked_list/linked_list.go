package doubly_linked_list

import "fmt"

type DoublyNode struct {
	prev *DoublyNode
	next *DoublyNode
	val  interface{}
}

type DoublyLinkedList struct {
	head *DoublyNode
	tail *DoublyNode
	len  int
}

func (d DoublyLinkedList) String() string {
	nodesStr := ""
	currNode := d.head
	for i := 0; i < d.len; i++ {
		nodesStr += fmt.Sprintf("%v", currNode)
		currNode = currNode.next
	}
	return fmt.Sprintf("Doubly linked list with len: %d, nodes: %s", d.len, nodesStr)
}

func (d *DoublyLinkedList) Length() int {
	return d.len
}

func (d *DoublyLinkedList) checkIndexOutOfRange(index int) error {
	if index < 0 {
		return fmt.Errorf("index %d < 0", index)
	}
	if index >= d.len {
		return fmt.Errorf("index %d is bigger than list lenght: %d", index, d.len)
	}
	return nil
}

func (d *DoublyLinkedList) traverseToIndex(index int) (*DoublyNode, error) {
	err := d.checkIndexOutOfRange(index)
	if err != nil {
		return nil, err
	}
	if index > d.len/2 {
		currNode := d.tail
		for i := d.len - 1; i > index; i-- {
			currNode = currNode.prev
		}
		return currNode, nil
	} else {
		currNode := d.head
		for i := 0; i < index; i++ {
			currNode = currNode.next
		}
		return currNode, nil
	}
}

func (d *DoublyLinkedList) Append(value interface{}) {
	newNode := DoublyNode{val: value}
	if d.len == 0 {
		d.head = &newNode
	} else {
		newNode.prev = d.tail
		d.tail.next = &newNode
	}
	d.tail = &newNode
	d.len++
}

func (d *DoublyLinkedList) Insert(index int, value interface{}) error {
	// need to traverse to the node behind the inserted index
	if index == d.len {
		d.Append(value)
		return nil
	}
	currNode, err := d.traverseToIndex(index)
	if err != nil {
		return err
	}
	newNode := DoublyNode{val: value}
	if index == 0 {
		d.head = &newNode
	} else {
		newNode.prev = currNode.prev
		currNode.prev.next = &newNode
	}
	currNode.prev = &newNode
	newNode.next = currNode
	d.len++
	return nil
}

func (d *DoublyLinkedList) Get(index int) (interface{}, error) {
	currNode, err := d.traverseToIndex(index)
	if err != nil {
		return nil, err
	}
	return currNode.val, nil
}

func (d *DoublyLinkedList) Pop(index int) (interface{}, error) {
	currNode, err := d.traverseToIndex(index)
	if err != nil {
		return nil, err
	}

	if currNode == d.tail {
		d.tail = currNode.prev
	}
	if currNode == d.head {
		d.head = currNode.next
	}

	if currNode.prev != nil {
		currNode.prev.next = currNode.next
	}
	if currNode.next != nil {
		currNode.next.prev = currNode.prev
	}

	d.len--
	return currNode.val, nil
}

func (d *DoublyLinkedList) Clear() {
	d.head = nil
	d.tail = nil
	d.len = 0
}
