package doubly_linked_list

import (
	"testing"
)

func generateDoublyLinkedList(lend int) DoublyLinkedList {
	if lend <= 0 {
		return DoublyLinkedList{}
	}
	var nodes []DoublyNode
	nodes = append(nodes, DoublyNode{val: 0})
	for i := 1; i < lend; i++ {
		nodes = append(nodes, DoublyNode{val: i})
	}
	for i := 1; i < lend; i++ {
		nodes[i-1].next = &nodes[i]
		nodes[i].prev = &nodes[i-1]
	}
	d := DoublyLinkedList{head: &nodes[0], tail: &nodes[lend-1], len: lend}
	return d
}

func TestDoublyLinkedList_Get(t *testing.T) {
	d := generateDoublyLinkedList(0)
	for i := 0; i < 10; i++ {
		_, err := d.Get(i)
		if err == nil {
			t.Fatalf("Accept Get while len is 0")
		}
	}

	for dlen := 1; dlen < 10; dlen++ {
		d := generateDoublyLinkedList(dlen)
		for i := 0; i < dlen; i++ {
			value, err := d.Get(i)
			if err != nil {
				t.Fatalf("Doubly linked list with len:%d Get method returned error: %v", dlen, err.Error())
			}
			if value.(int) != i {
				t.Fatalf("Doubly linked list Get method not working")
			}
		}

		indexBelow := -1
		indexAbove := dlen
		_, err := d.Get(indexBelow)
		if err == nil {
			t.Fatalf("Doubly linked list Get method doesn't return error on indices below 0")
		}
		_, err = d.Get(indexAbove)
		if err == nil {
			t.Fatalf("Doubly linked list Get method doesn't return error on indices above linked list len")
		}
	}
}

// Depends on TestDoublyLinkedList_Get to work correctly
func TestDoublyLinkedList_Append(t *testing.T) {
	appendLen := 10
	for defaultLen := 0; defaultLen < 10; defaultLen++ {
		d := generateDoublyLinkedList(defaultLen)
		for app := 0; app < appendLen; app++ {
			d.Append(1000 + app)
			if d.len != app+defaultLen+1 {
				t.Fatalf("Error in calculating linked list length while appending. len: %d, len must be: %d",
					d.len, app+defaultLen+1)
			}
		}
		for index := 0; index < defaultLen; index++ {
			value, err := d.Get(index)
			if err != nil {
				t.Fatalf("Error in getting values after appending to the doubly linkedlist")
			}
			if value != index {
				t.Fatalf("Changing linkedlist values in append!")
			}
		}
		for index := defaultLen; index < defaultLen+appendLen; index++ {
			value, err := d.Get(index)
			if err != nil {
				t.Fatalf("Error in getting appended values")
			}
			if value != 1000+index-defaultLen {
				t.Fatalf("Wrong appended values!")
			}
		}
	}
}

// Depends on TestDoublyLinkedList_Get to work correctly
func TestDoublyLinkedList_Insert(t *testing.T) {
	appendLen := 10
	for defaultLen := 0; defaultLen < 10; defaultLen++ {
		d := generateDoublyLinkedList(defaultLen)
		for app := -5; app < defaultLen+appendLen+5; app++ {
			err := d.Insert(app, -app)
			if app < 0 || app > d.len {
				if err == nil {
					t.Errorf("No error while index %d out of range for %v", app, d)
				}
				continue
			}
			if err != nil {
				t.Errorf("Error %v while inserting data to doubly linkedlist", err.Error())
			}

			expectedLen := defaultLen + app + 1
			if d.len != expectedLen {
				t.Errorf("Expected linked list length to be: %d, is: %d", expectedLen, d.len)
			}
			val, err := d.Get(app)
			if err != nil {
				t.Errorf("Problem while getting %dth value from %v", app, d)
			}
			val = val.(int)
			expectedVal := -app
			if val != expectedVal {
				t.Errorf("expected value %d is not %d. index: %d, list: %v", expectedVal, val, app, d)
			}

		}
	}
}

func TestDoublyLinkedList_Length(t *testing.T) {
	appendLen := 10
	for defaultLen := 0; defaultLen < appendLen; defaultLen++ {
		d := generateDoublyLinkedList(defaultLen)
		if d.Length() != d.len {
			t.Fatalf("Wrong doubly linked list length method: expect: %d, is: %d", d.len, d.Length())
		}
	}
}

// Depends on TestDoublyLinkedList_get to work correctly
func TestDoublyLinkedList_Pop(t *testing.T) {
	for defaultLen := 0; defaultLen < 10; defaultLen++ {
		minIndex := -5
		maxIndex := defaultLen + 5
		for index := minIndex; index < maxIndex; index++ {
			d := generateDoublyLinkedList(defaultLen)
			lenBeforePop := d.len
			expectedLenAfterPop := lenBeforePop - 1
			val, err := d.Pop(index)
			if index < 0 || index >= defaultLen {
				if err == nil {
					t.Errorf("No error while index %d out of range %v", index, defaultLen)
				}
				continue
			}
			val = val.(int)
			if val != index {
				t.Errorf("Wrong poped value: %d in index: %d for list: %v", val, index, d)
			}
			if d.len != expectedLenAfterPop {
				t.Errorf("Wrong lenght after pop. expected: %d, got: %d, in: %v", expectedLenAfterPop, d.len, d)
			}
		}
	}
}

// Depends on TestDoublyLinkedList_get to work correctly
func TestDoublyLinkedList_Clear(t *testing.T) {
	for defaultLen := 0; defaultLen < 10; defaultLen++ {
		d := generateDoublyLinkedList(defaultLen)
		d.Clear()
		if d.len != 0 {
			t.Errorf("Doubly linked list Clear method not working correctly. linkedlist after clear: %v", d)
		}
	}
}
